## ¿Qué es Site Building?
Site building hace referencia a todo lo que podemos hacer desde la interfaz de usuario de Drupal sin necesidad de tocar código. Drupal cuenta con una gran cantidad de módulos contribuidos que permiten extender su funcionalidad y construir lo que imagines

### Para ingresar al sitio
Si aún no estas logueada/o como Admin podes entrar desde la URL
```
https://[URL_DE_MI_SITIO]/user
```
Te pedirá los datos de usuaria/o y la password ingresados en la instalación de Drupal.

![Screenshot](img/drupal-instalado.jpg)
*Usuaria/o admin logueada/o*

Una vez logueada/o, desde el menú <strong>Estructura</strong> podemos acceder a administrar mucho de lo que te aprendimos en el capítulo <strong>Conociendo Drupal</strong>: los Bloques (Diseño de bloque), Formularios de contacto, Menús, Modos de visualización, Taxonomías, Tipos de comentario, Tipos de contenido y Vistas

### Modificar estructura

Para acceder al apartado de la estructura de nuestro sitio lo haremos desde la siguiente url:
```
https://[URL_DE_MI_SITIO]/admin/structure
```
![Screenshot](img/estructura.jpg)


### Ver contenidos
Podremos gestionarlos clickeando en Tipos de contenidos (Content Types) o desde la siguiente url:

```
https://[URL_DE_MI_SITIO].lndo.site/admin/content
```
Para crear un nuevo contenido debemos elegir el tipo que queremos crear. <br/  >
Artículo o Página básica, son los tipos de contenido que vienen por defecto con Drupal 8.<br/  >
Luego podremos crear mas tipos de contenido con la estructura que necesitemos<br/  >

### Administrar y crear contenidos
Para agregar nuevo contenido lo haremos desde:

```
https://[URL_DE_MI_SITIO].lndo.site/node/add
```
![Screenshot](img/seleccionar-tipo-contenido-crear.jpg)
*Tipos de contenidos por defecto de Drupal

### Crear artículo

Usaremos el tipo de contenido Artículo (article)<br/  >
Luego de completar la información del título y el cuerpo del artículo, veremos en la parte derecha de la pantalla las <strong>Opciones de promoción</strong>: para expecificar si queremos que se muestre en la portada (en decir, en el inicio) o como destacado en la parte superior de un listado.

![Screenshot](img/crear-articulo.jpg)

### Cambiar apariencia
Podremos activar o desactivar los temas instalados. También seleccionar uno para que funcione como predeterminado y  setear un logo para nuestro sitio web.

### Vista de Apariencia
```
https://[URL_DE_MI_SITIO].lndo.site/admin/appearance
```
Usaremos el tema de la comunidad de Drupal que usa [Bootstrap 3](https://getbootstrap.com/docs/3.3/){target=_blank}: una biblioteca  de código abierto para diseño de sitios y aplicaciones web que nos facilitará el trabajo para que nuestro sitio web se ve bien.

Para ello, tenemos que ir al [repositorio de temas](http://drupal.org/project/themes){target=_blank} de Drupal.
En los buscadores es importante usar los filtros para buscar paquetes compatibles con la versión de Drupal 8 que instalamos.

Instalaremos el siguiente [tema](https://www.drupal.org/project/bootstrap_barrio){target=_blank}.
En esa misma página, debajo de todo en la sección Downloads, dirá algo así:
```
 8.x Stable release covered by the Drupal Security Team released Date <br/  >
✓ Recommended by the project’s maintainer.<br/  >
⬇ Download tar.gz (23.78 KB) | zip (49.29 KB) <br/  >
```
Haremos click derecho sobre el link Download tar.gz
para copiar la dirección de enlace [link](https://ftp.drupal.org/files/projects/bootstrap_barrio-8.x-4.28.tar.gz){target=_blank}:
```
https://ftp.drupal.org/files/projects/bootstrap_barrio-8.x-4.28.tar.gz
```
### Instalar tema Bootstrap
Desde esta URL podremos instalarlo:
```
https://[URL_DE_MI_SITIO].lndo.site/admin/appearance/install
```
![Screenshot](img/instalando-ftp-bootstrap.jpg)

Pegamos el link donde se solicita la URL y hacemos clic en Instalar.
Drupal hará lo necesario para instalar el tema.

![Screenshot](img/instalando-bootstrap.jpg)


### Seleccionar tema

Ahora sólo tenemos que activar el tema y establecerlo como predeterminado.

![Screenshot](img/selecionar-tema.png)

Después de este paso, podemos ir a la página principal para ver el nuevo diseño.  Felicitaciones!

![Screenshot](img/vista-de-noticias.jpg)


### Subir nuestro logo

Desde el listado de temas haremos click en "Configuración" del tema que tenemos por defecto: En nuestro caso, el de   Bootstrap.
Podés acceder directamente desde la URL
```
https://[URL_DE_MI_SITIO].lndo.site/admin/appearance/settings/bootstrap
```

![Screenshot](img/cargar-logo.png)

En la sección "Global Settings" hacemos clic en la pestaña "imagen del logotipo", destildamos la opción "Utilizar el logotipo proporcionado por el tema" y podremos cargar la imagen del logotipo que disponemos y hacemos clic en "Guardar configuración".

![Screenshot](img/inicio-con-logo.png)


## Instalaremos el módulo Social Media
Este módulo nos permitirá que las personas que visiten nuestro sitio web puedan compartir en las redes sociales nuestros artículos y contenidos.
Desde el menú Extender podemos agregar y/o gestionar módulos.

Podés acceder directamente desde la URL
```
https://[URL_DE_MI_SITIO].lndo.site/admin/modules
```
Buscaremos en el [respositorio](http://drupal.org/project/modules){target=_blank} de la comunidad de Drupal el módulo [Social Media Links](https://www.drupal.org/project/social_media_links){target=_blank}

De la misma forma que con el tema, debajo de todo en Downloads:
```
 8.x Stable release covered by the Drupal Security Team released Date
✓ Recommended by the project’s maintainer.
⬇ Download tar.gz (23.78 KB) | zip (49.29 KB)
```
Haremos click derecho sobre el link Download tar.gz
para copiar la dirección de enlace [link](https://ftp.drupal.org/files/projects/social_media_links-8.x-2.6.tar.gz){target=_blank}.

```
https://ftp.drupal.org/files/projects/social_media_links-8.x-2.6.tar.gz
```

En nuestro Drupal, pegaremos la url donde lo indique e instalaremos el módulo.
![Screenshot](img/instarlar-social-media.jpg)

Luego tendremos que buscarlo por su nombre (Social Media) para habilitarlo y poder empezar a usarlo!

Desde Administración → Extender<br/  >
Localizaremos el módulo Social Media Links Block en el listado y haremos clic en Activado.<br/  >


![Screenshot](img/instalar-modulo.jpg)

Guardamos la configuración.<br/  >

![Screenshot](img/habilitar-modulos-social.jpeg)

Para usarlo tenemos que configurar nuestro primer bloque.<br/  >
En el área de administración de bloques:<br/  >
Administración → Estructura → Diseño de bloque

![Screenshot](img/listado-block-layout.jpg)

Lo ubicaremos en región Secundaria.

![Screenshot](img/social-block.jpg)

Colocar bloque y se desplegarán las opciones de los links para ser completadas.

![Screenshot](img/ubicar-bloque-en-posicion.jpg)

Podemos agregar la opciones que se vea en el inicio del sitio agregando la opción `<front>` en Pages.

## Instalaremos un módulo con composer

Esta opción es más rápida y segura. Vamos a instalar "Admin Toolbar", un módulo que nos sirve para mejorar el menú administrativo de nuestro Drupal 8.

Para esto, desde la consola ejecutaremos el siguiente comando:
```
lando composer require drupal/admin_toolbar
```
Luego habilitaremos el módulo instalado usando drush:
```
lando drush en admin_toolbar -y
```
Listo! podremos acceder a las distintas secciones desde un menú desplegable.

![Screenshot](img/menu-admin-tools.png)


## Crear una vista

Ahora crearemos una vista para ver las últimas noticias.  
Si te interesa aprender más sobre vistas de Drupal 8 puedes ingresar [aquí](http://www.isipedia.com/informatica/apuntes-sobre-drupal-8/entendiendo-las-vistas-en-drupal-8){target=_blank}.

```
https://[URL_DE_MI_SITIO].lndo.site/admin/structure/views
```
Aquí veremos todas las vistas que ya tenemos creadas.
Hacemos clic en "Agregar vista".

![Screenshot](img/vistas-creadas.png)


Agregar un nombre a la vista y una descripción a la misma que nos permitirá entender el contenido de la misma.

En "Opciones de vista" seleccionaremos "Contenido" y tipo de contenido "Artículo". También podemos configurar cómo ordenar los contenidos seteando el valor desde "ordenado por". En nuestro caso seleccionaremos "Más recientes primero".


![Screenshot](img/creando-vista.png)


En "Optiones de página" tildaremos "Crear una página". Agregaremos el "Título de página" y la ruta en la cual estará disponible la vista.


## Comandos útiles

Será muy importante ir borrando la caché para ver reflejado los cambios en nuestro sitio. Desde consola podés hacerlo con:
```
lando drush cr
```

Comando | Descripción
------------ | -------------
lando drush en [nombre del módulo] -y | Activar un módulo
lando drush pmu [nombre del módulo] | Desactivar un módulo
lando drush sql-dump > [ficherodump.sql] | Generar un dump de la base de datos
lando drush sql-cli < [ficherodump.sql] | Importar el contenido de un dump
lando drush sql-query "SELECT * FROM node" | Realizar consultas sobre la base de datos
lando composer require drupal/bootstrap_barrio | Instalar un modulo
lando php | Ejecuta comandos php
lando mysql | Shell de MySQL en un servicio de base de datos
lando drupal | Ejecuta los comandos de la consola de drupal
lando drush | Ejecuta comandos drush
lando composer  | Ejecuta comandos composer

## ¡Creaste un gran sitio web de noticias!
![Screenshot](img/noticias-para-blog.png)

¡Felicitaciones!
