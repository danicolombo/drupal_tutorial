<iframe width="100%" height="315" src="https://www.youtube.com/embed/gETi7R-15fE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<br>

## Vamos a conocer un poco más de Drupal

### El núcleo o core
Es la parte más importante para el funcionamiento de Drupal y  algunas de sus funciones se pueden usar de forma directa a través de las llamadas APIs de Drupal, pero esto lo dejaremos para más adelante. Si te interesa, puedes encontrar más informacion [aqui](https://www.drupal.org/docs/8/api){target=_blank}.

### Los módulos
Sirven para sumar funcionalidades al núcleo de Drupal.
En este tutorial usaremos un módulo contribuido (realizado por la comunidad de Drupaleres) llamado [Social Media Links](https://www.drupal.org/project/social_media_links){target=_blank}, y crearemos nuestro propio módulo para recibir mensajes a traves de Whatsapp.

### Administración
Hay una barra de herramientas situada en la parte superior de la sitio desde la cual podemos acceder a diferentes opciones desde la URL /admin.

### Entidades y campos
Podemos crear entidades y se les puede añadir campos de información de diferentes tipos (texto, fecha, imagen, archivo, número, y muchos otros más). Algunas entidades en Drupal que ya vienen por defecto son los Nodos o Tipos de contenido (lo que usaremos para crear contenidos), las Taxonomias (las categorias para esos contenidos), los Usuarios (los distintos tipos de roles que pueden administras/editar/etc el sitio), entre otros.

### Nodos y tipos de contenidos
El tipo de contenido básico se denomina nodo. Podríamos decir que los tipos de contenido son como plantillas o moldes a partir de los cuales crearemos nuestros contenidos específicos.
Por ejemplo, podemos tener el tipo de contenido Noticia y crear a partir de el, los contenidos: Noticia 1, Noticia 2, Noticia 3. Cada una de estas noticias serán instancias de ese tipo de nodo.

### Bloques
Los bloques (blocks en inglés) son como contenedores que se colocan en distintas zonas (o regiones) del sitio.
Por ejemplo, podemos colocar un bloque al costado de nuestro sector principal de forma que funcione como un menú lateral.
Podemos colocar un bloque en el footer con los links de contacto, etc etc!

### Temas
El tema (theme en inglés) define el diseño. Hay un [repositorio de temas](http://drupal.org/project/themes){target=_blank} de Drupal de temas libres para usar, que pueden ser descargados y luego readaptados en nuestro sitio.

### Usuarios, roles y permisos
Podremos controlar el acceso de los distintos tipos de usuarios asignándoles roles y permisos. Básicamente un rol es un conjunto de permisos.

### Taxonomías
Con ellas podremos categorizar los contenidos. Tenemos los vocabularios (o categorías) y los términos (o etiquetas). Cada vocabulario puede contendor a uno o más términos, y en nuestro sitio podemos tener uno o más vocabularios.

### ¡A construir nuestro sitio web con Drupal 8!
