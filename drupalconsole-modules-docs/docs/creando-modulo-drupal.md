![Screenshot](img/drupal-console.png)

[Drupal Console](https://drupalconsole.com/) es una herramienta muy útil que te permitirá administrar tu sitio web, instalar, modificar y eliminar librerías o temas y muchas otras cosas.

Si queres más info del uso de Drupal Console podés clickear [aquí](https://metadrop.net/articulos/comando-drupal-console){target=_blank}

Usaremos Drupal console para crear un módulo desde el cual quienes visiten nuestro sitio web puedan ponerse en contacto con nosotras/os a través de WhatsApp.

## Crearemos el módulo con generate:module

Desde consola:
```
lando drupal generate:module \
--module="misitio_whatsapp" \
--machine-name="misitio_whatsapp" \
--module-path="modules" \
--description="Integración de icono chat whatsapp, telegram y messenger" --core="8.x" \
--package="Custom" \
--module-file \
--twigtemplate \
--no-interaction
```
## Crearemos un form con generate:form:config

Éste será el formulario para configurar nuestro Whatsapp.
```
lando drupal generate:form:config \
--module="misitio_whatsapp" \
--class="ConfigWhatsappForm" \
--form-id="config_whatsapp_form" \
--config-file \
--inputs='"name":"whatsapp", "type":"textfield", "label":"Número de Whatsapp", "options":"", "description":"Ingrese su numero de whatsapp Ej: 541111111111", "maxlength":"64", "size":"64", "default_value":"none", "weight":"0", "fieldset":""' \
--path="/admin/config/whatsapp/configuracion" \
--no-interaction
```
## Crearemos un yml para linkearlo, es decir, agregarlo al menú desde donde administramos nuestro sitio web.
Crear el archivo: misitio_whatsapp.links.menu.yml
```
misitio_whatsapp.admin_config_system:
title: Whatsapp
route_name: misitio_whatsapp.config_whatsapp_form
parent: system.admin_config
description: 'Configure whatsapp.'
weight: -20
```

## Ahora tenemos que modificar el archivo misitio_whatsapp.routing.yml
```
misitio_whatsapp.config_whatsapp_form:
path: '/admin/config/whatsapp/configuracion'
defaults:
_form: '\Drupal\misitio_whatsapp\Form\ConfigWhatsappForm'
_title: 'ConfigWhatsappForm'
requirements:
_permission: 'administer site configuration'
options:
_admin_route: TRUE
```

## Generar bloque para nuestro módulo
Ya vimos que los bloques son como contenedores que se colocan en distintas zonas (o regiones) del sitio web.
Desde consola:
```
./vendor/bin/drupal generate:plugin:block --module="misitio_whatsapp" --class="WhatsappBlock" --plugin-id="whatsapp_block" --theme-region="footer" --no-interaction
```

## Reescribir el bloque
Como vimos al inicio del tutorial, HTML sirve para maquetar nuestro sitios. Drupal en cambio, utiliza unas plantillas  llamadas [Twig](https://es.wikipedia.org/wiki/Twig_(motor_de_plantillas)){target=_blank}

Reescribiremos el twig del bloque. Para ello, debés entrar a las carpeta de nuestro proyecto:
```
/themes/misitio/templates/block/block--whatsapp-block.html.twig
```

Y modificaremos con Atom lo siguiente:

```
<section{{ attributes.addClass(classes) }}>

{{ title_prefix }}

{{ title_suffix }}

{% block content %}

&lt;meta name="viewport" content="initial-scale=1.0, minimum-scale=1.0, user-scalable=yes"/&gt;

&lt;!-- Font Awesome --&gt;
        &lt;link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"&gt;

{% if whatsapp %}
        &lt;a href="https://api.whatsapp.com/send?phone={{ whatsapp }}&text=Hola App!" target="_blank" class="whatsapp" data-aos="fade-up" id="whatsapp"&gt;
          &lt;i class="fab fa-whatsapp"&gt;&lt;/i&gt;
        &lt;/a&gt;
{% endif %}

{% endblock %}
&lt;/section&gt;
```

## Le agregaremos ciertos estilos css para que se vea bien
```
.whatsapp{
    width:50px;
    height:50px;
    background-color:#25d366;
    color:#FFF;
    border-radius:50px;
    position: fixed;
    bottom: 15px;
    left: 15px;
    font-size:30px;
    text-align: center;
    box-shadow: 4px 4px 10px rgba(0, 0, 0, 0.4);
    z-index: 1000;
    padding-top: 4px;
}
.fa-whatsapp{
    color: #FFF;
}
```

## Crearemos un hook en .theme
Qué es un hook? Los hooks permiten que se altere y/o extienda el comportamiento del núcleo o core de Drupal o de otro módulo.
En el archivo my_theme.theme agregaremos debajo de todo este hook para mostrar la variable del número de whatsapp:
```

function misitio_preprocess_block(&$variables) {
	$variables['whatsapp'] = \Drupal::config('misitio_whatsapp.configwhatsapp')->get('whatsapp');
}
```

Ya tenemos listo nuestro módulo!

## Sólo nos queda instalar nuestro módulo y habilitarlo con Drush

Para hacerlo debemos ejecutar el siguiente comando desde la consola:
```
lando drush en misitio_whatsapp -y
```
Esto habilitará el módulo y lo podremos ver en nuestro menú de configuración como "Configure whatsapp".

## Ahora podemos usarlo, vamos a agregarlo como bloque
Agregaremos el bloque misitio_whatsapp en la región final o footer de nuestro sitio, donde suelen estar los datos de contacto.

Borramos la caché del sitio para poder ver todos los cambios realizados con:
`lando drush cr`

## ¡Excelente!

![Screenshot](img/whatsapp.png)

Podemos ver nuestro ícono de WhatsApp para mandar/recibir mensajes!
