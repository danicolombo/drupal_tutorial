##Creando nuestro entorno de desarrollo
¡Manos a la obra!

Antes que todo instalaremos herramientas extremadamentes útiles que nos ayudaran a mantener un entorno de desarrollo ordenado en nuestra computadora. <b>¡Empezar con la mejor configuración posible te ahorrará muchos problemas en el futuro!</b>

Para crear el sitio web vamos a necesitar tener instalados una serie de programas: php, mysql o mariadb, apache o nginx, varnish, etc. <b>Todo esto puede convertirse en una gran molestia si no lo tenemos en cuenta desde el principio</b> y si no disponemos de un entorno adaptado para cada proyecto.

<b>Una buena práctica consiste en usar un entorno de desarrollo</b> que sea lo más parecido posible al entorno donde va a estar publicada nuestra web, para esto vamos a utilizar herramientas como [Docker](https://www.docker.com/){target=_blank} y [Lando](https://docs.lando.dev/){target=_blank}.

##¿Qué es Docker?
Es un proyecto de código abierto que nos permitirá crear contenedores con todo lo necesario para instalar Drupal 8.
Esto quiere decir que cualquier cambio que hagas en un sitio web no afectará a ningún otro que sitio estés desarrollando. Genial, ¿no?

<h5>Instalación</h5>

* Aquí podés leer más sobre [Docker](https://docs.docker.com/get-docker/){target=_blank}.
* [Linux Docker engine](https://docs.docker.com/engine/install/){target=_blank}.
* [Docker for Mac](https://docs.docker.com/docker-for-mac/#/what-to-know-before-you-install){target=_blank}.
* [Docker for Windows](https://docs.docker.com/docker-for-windows/#/what-to-know-before-you-install){target=_blank}.

##¿Por qué Lando?
Porque queremos un asistente de Docker que nos permita crear el entorno de desarrollo rápidamente y poder cambiar configuraciones en cualquier momento sin perder tiempo. <b>Lando es una herramienta de código abierto basada en los contenedores de Docker</b>.

Aquí la [documentación de Lando](https://docs.lando.dev/basics/installation.html){target=_blank}.

<h5>Compatibilidad de Lando</h5>
* macOS 10.14 o superior
* Windows 10 Pro+ o equivalente (ej. Windows 10 Enterprise) con * Hyper-V
* Linux con kernel 4.x o superior

Es necesario tener Docker ya instalado

<h5>Instalación GNU/Linux</h5>

* Descargar la última versión de [Lando](https://github.com/lando/lando/releases){target=_blank}. <br/  >
Archivo .deb, .pacman ó .rpm
* Instalarlo desde la terminal con:
`sudo dpkg -i lando-v3.0.3.deb`

<h5>Instalación Windows</h5>
Es necesario tener al menos Windows 10 Professional  <br/  >
* Descargar la última versión de [Lando](https://github.com/lando/lando/releases){target=_blank}. <br/  >
Archivo .exe <br/  >
* Seguí las instrucciones

<h5>Instalación MacOS</h5>
* Descargar la última versión de [Lando](https://github.com/lando/lando/releases){target=_blank}. <br/  >
Archivo .dmg <br/  >
* Montar el DMG doble-clickeandolo
* Lo mismo con LandoInstaller.pkg
* Seguí las instrucciones


<h5>¿Como funciona Lando y qué son las recetas?</h5>
Las recetas de Lando tienen todo lo que necesitas correr para desarrollar y testear tu proyecto.<br/ >
Estas configuraciones estarán en un archivo dentro del proyecto llamado:
`
.lando.yml
` <br/  >
Notaras el . adelante del nombre del archivo, esto significa que es un archivo oculto. No te preocupes, ahora aprenderemos a crearlo!

##Creando receta con Lando

Comenzaremos creando un directorio en donde trabajaremos con el proyecto. Abrimos nuestra consola y ejecutamos el siguiente comando.

```
mkdir midrupal8
```
Para entrar al directorio o carpeta del proyecto recién creado, ejecutamos desde la consola: <br/  >
```
cd midrupal8
```
Ahora inicializaremos nuestro contenedor para nuestro entorno de desarrollo usando una receta Lando con la última versión de Drupal 8. Lo haremos ejecutando el siguiente comando desde la consola: <br/  >
```
lando init \
  --source remote \
  --remote-url https://ftp.drupal.org/files/projects/drupal-8.9.0.tar.gz \
  --remote-options="--strip-components 1" \
  --recipe drupal8 \
  --webroot . \
  --name my-first-drupal8-app
```

 <br/  >

<strong>Excelente!</strong>

```
NOW WE'RE COOKING WITH FIRE!!!
Your app has been initialized!

Go to the directory where your app was initialized and run `lando start` to get rolling.
Check the LOCATION printed below if you are unsure where to go.

Oh... and here are some vitals:

NAME      my-first-drupal8-app                                
LOCATION  /ruta-desde-donde-lanzaste-lando              
RECIPE    drupal8                                             
DOCS      https://docs.devwithlando.io/tutorials/drupal8.html

```
Esto descargara los archivos que necesita para correr un Drupal 8 y creará un archivo oculto .lando.yml con algunas configuraciones para nuestro contenedor que se verá de la siguiente forma:
```
name: my-first-drupal8-app
recipe: drupal8
config:
  webroot: .
```

<h5> ¡Podemos configurar nuestro entorno a gusto!</h5>
Si querés podés agregar algunas herramientas más a la receta que acabamos de crear que nos vendrán al pelo para crear nuestro sitio web, para eso modificaremos el archivo .lando.yml
Usaremos un editor de texto para esto, podés usar Vi, Vim, Emacs, Atom, VSCodium o el que más te guste! <br/  >

<h5>Abrimos el archivo .lando.yml con Atom</h5>
Y reemplazamos su contenido con la siguiente información:
```    
name: my-first-drupal8-app
recipe: drupal8
config:
  webroot: .
  drush: ^8
  xdebug: true
services:
  appserver:
    build:
      - composer install   

```
Recordá que podés editar el nombre del proyecto (my-first-drupal8-app en este caso) <br/  >
Usá _ para los espacios en blanco en caso que lo necesites.

En esta receta estamos usando <strong>PHP version 7.3</strong>, <strong>Apache</strong> como servidor y <strong>mysql</strong> como base de datos para guardar los contenidos. Lando configurará automáticamente esta base de datos con un usuario y una contraseña y también establecerá una variable a la que podrás recurrir para buscar esa información: <br/  >
lando info <br/  >
También agregamos <strong>Drush</strong> que es una línea de comando que te permite administrar tu página Drupal de manera rápida y fácil. Es el acrónimo entre Drupal y shell :)
Otra herramienta que agregamos es <strong>Composer</strong>: un gestor de paquetes PHP para manejar dependencias y librerías.
Existen diferentes opciones para configurar nuestra receta de drupal 8, aquí podrás ver toda la [documentación](https://docs.lando.dev/config/drupal8.html#configuration){target=_blank} o incluso, podés crear tu propia receta.


<h5>Ya casi estamos</h5>
Para probar nuestra receta Lando desde el directorio o carpeta de nuestro sitio, ejecutaremos por consola<br/  >
```
lando start
```
Lando creará el contenedor necesario para correr nuestro Drupal 8.

Si todo va bien, nos mostrará una serie de links para entrar desde el navegador:

```
BOOMSHAKALAKA!!!

Your app has started up correctly.
Here are some vitals:

 NAME            nombre_del_proyecto                       
 LOCATION        /ruta-desde-donde-lanzaste-lando        
 SERVICES        appserver, database                       
 APPSERVER URLS  https://localhost:32773                    
                 http://localhost:32774                     
                 http://my-first-drupal8-app.lndo.site:8000
                 https://my-first-drupal8-app.lndo.site   
```
Si ingresamos a esos links no veremos nada! Pero está vivo, tenemos nuestro Lando corriendo y aprovecharemos Composer (que acabamos de configurar en nuestra receta Lando) para poder crear los archivos necesarios de Drupal.

### Voilà!
Si entramos a nuestra URL del contenedor: `https://my-first-drupal8-app.lndo.site`<br/  >
Allí nos encontraremos con el instalador web de Drupal 8. <br/  >

##Muy bien! terminemos de instalar nuestro sitio Drupal 8!

Te aparecerá una pantalla con fondo azul con las opciones de configuración para instalarlo.

![Screenshot](img/instalacion-drupal-web.jpg)

## Instalación via Web

En este tutorial vamos a elegir una configuración básica del sitio para empezar a construir nuestra web!
```
Elegiremos la configuración del lenguaje: por defecto en inglés
Elegiremos la configuración del profile: estándar

En la configuración de la base de datos vamos a completar con la siguiente información:

* Tipo de la base de datos: mysql
* Nombre de la database: drupal8
* Usuario: drupal8
* Password: drupal8
* Hostname: database
* port: 3306

Finalmente vamos a completar la informacion del sitio con mail, nombre de usuario, password.
Recuerda esos valores que luego los necesitaremos.
```
Podras acceder desde el navegador a tu nuevo sitio Drupal desde
```
https://my-first-drupal8-app.lndo.site/user
```
Ingresando los datos de usuario y passwords que creaste en la instalación.


## Comandos útiles de Lando

* `lando start`<br/  >
* `lando restart`<br/  >
* `lando rebuild`<br/  >
* `lando info`<br/  >

<h5>Para dejar de correr los containers creados por Lando</h5>
* `lando poweroff`
